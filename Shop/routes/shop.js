var express = require('express');
var router = express.Router();
var mongojs = require('mongojs');
var mongodb = require('mongodb');
var mongoClient = mongodb.MongoClient;
var url = "mongodb://127.0.0.1:27017"

router.get('/list1', function(req, res, next) {
    var views = req.body.view
    mongoClient.connect(url, function (err, databases) {
        if (err) { console.log(err) }
        else {
            var dtb = databases.db("sample")
            var prdtable = dtb.collection("product")
            prdtable.find().toArray(function (err, result) {
                if (err) {
                    res.send("Error")
                }
                else {
                    console.log(result)
                    res.render('list', {data: result})
                    // res.send(result);
                }
            })
        }
    })
});

router.get('/create', function(req, res, next) {
    res.render('create');
});

router.post('/create1', function(req, res, next) {
    var prod = req.body;
    mongoClient.connect(url, { useNewUrlParser: true, useUnifiedTopology: true }, function (err, databases) {
        if (err) {
            console.log(err)
        }
        else {
            console.log("DATA:"+databases);
            var dtb = databases.db("sample")
            var prdtable = dtb.collection("product")

            var data = { prodId:prod.prodId, prodname: prod.prodName, prodPrice: prod.prodPrice, prodQuant: prod.prodQuant }
            prdtable.insertOne(data, {upsert: true}, function (err, result) {
                if (err) {
                    res.send("insertion failed")
                }
                else {
                    console.log("TABLE:"+prdtable);
                    res.send("data inserted")
                }
            })
        }
    })
});

router.get('/delete', function(req, res, next) {
    res.render('delete');
});

router.post("/delete1", function (req, res) {
    var prodId = req.body.prodId

    mongoClient.connect(url, function (err, databases) {
        if (err) {
            res.send("Not deleted")
        }
        else {
            var dtb = databases.db("sample")
            var prdtable = dtb.collection("product")
            var data = { prodId: prodId }
            prdtable.deleteOne(data, function (err, result) {
                if (err) {
                    res.send("Fail")
                }
                else {
                    res.send("Success")
                }
            })
        }
    })
})
router.get('/modify', function(req, res, next) {
    res.render('modify');
});

router.post('/modify1', function(req, res, next) {
    var data = req.body

    mongoClient.connect(url, function (err, databases) {
        if (err) { 
            console.log(err) 
            res.send("Not Modified")
        }
        else {
            var dtb = databases.db("sample")
            var prdtable = dtb.collection("product")
            var myquery = { prodId: data.prodId };
            var newvalues = { $set: {prodname: data.prodName, prodPrice: data.prodPrice, prodQuant: data.prodQuant } };
            prdtable.updateOne(myquery, newvalues, function(err, result) {
                if (err) {
                    res.send("Fail")
                }
                else {
                    res.send("Success")
                    console.log("1 document updated");
                }
              });
        }
    })    
});

module.exports = router;

